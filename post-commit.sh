#!/usr/bin/env bash

set -e

./gradlew lint
./gradlew sonarqube

# AMP - Android Music Player

An Android Music Player application developed for the Mobile Device Programming coursework.

Coursework specification can be found in [g54mdp_coursework1.pdf](g54mdp_coursework1.pdf).

[Licensed under the terms of the GPLv3](LICENSE.md).

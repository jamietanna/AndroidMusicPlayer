package me.jvt.mdp.amp;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;

/**
 * AudioPlayer is a wrapper for the Android MediaPlayer class, so we don't tightly couple our
 * overall code to that of the MediaPlayer. This means down the line we can use another media player,
 * such as if we need different codec support, etc
 * <p/>
 * AudioPlayer is a Singleton, because we will only ever need a single instance of music playing at
 * once!
 */
public class AudioPlayer {

    private static AudioPlayer s_instance = null;
    private MediaPlayer m_player;

    protected AudioPlayer() {
    }

    public static AudioPlayer getInstance() {
        if (s_instance == null)
            s_instance = new AudioPlayer();

        return s_instance;
    }

    public void stop() {
        if (m_player != null) {
            // make sure we first stop the music playing
            m_player.stop();
            // then we release any resources required by the MediaPlayer, instead of waiting until
            // the garbage collection is called
            m_player.release();
            // finally set it to null, so we know it's not being used!
            m_player = null;
        }
    }

    public void play(Context c, String filePath) {
        // stop before we play another song, so we don't have multiple songs playing at once
        stop();

        try {
            Uri uri = Uri.parse("file:///" + filePath);
            Log.d(ApplicationConfiguration.TAG, uri.toString());
            m_player = new MediaPlayer();
            m_player.setDataSource(c, uri);
            m_player.prepare();
            m_player.start();
        } catch (IOException e) {
            // log to LogCat - not pretty, but a lot of major apps do similar. It definitely helps
            // when debugging
            Log.e(ApplicationConfiguration.TAG, e.getMessage(), e);
        }
    }

    public boolean isPlaying() {
        if (m_player == null)
            return false;
        else
            return m_player.isPlaying();
    }

    public boolean isStopped() {
        return m_player == null;
    }

    public void pause() {
        if (isPlaying()) {
            m_player.pause();
        } else {
            // TODO what if we pause something that isn't playing?
            m_player.start();
        }

    }

    /**
     * Get the current playing time
     *
     * @return current time in milliseconds, or -1 if not playing
     */
    public int getCurrentPosition() {
        if (m_player == null || !isPlaying())
            return -1;

        return m_player.getCurrentPosition();
    }


    /**
     * Set the current playing time
     *
     * @param currentPosition - time in milliseconds to seek to
     */
    public void setCurrentPosition(int currentPosition) {
        if (m_player == null)
            return;

        m_player.seekTo(currentPosition);
    }
}

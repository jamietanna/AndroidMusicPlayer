package me.jvt.mdp.amp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A playlist that is in a random order, compared to that of the original order given.
 */
public class ShufflePlaylist extends Playlist {
    public ShufflePlaylist() {
        super();
    }

    /**
     * Create a shuffled playlist, ensuring to keep currentSong at the beginning
     *
     * @param currentSong
     * @param songList
     */
    public ShufflePlaylist(Song currentSong, List<Song> songList) {
        // send our songList so we can get an internal copy for later
        super(songList);

        // make sure we have our currentSong on the top of the list - otherwise things get confusing
        // for the user
        List<Song> songListShuffled = new ArrayList<Song>();
        songListShuffled.add(currentSong);
        // reuse our internal copy of the song list (which has been duplicated from the parameter)
        // and shuffled a la @{ShufflePlaylist.setPlaylist}
        songListShuffled.addAll(m_songList);
        m_songList = songListShuffled;
    }

    /**
     * We override setPlaylist to force it to sort the array before saving it. By doing it at the
     * first step, we don't then have to deal with it again
     *
     * @param songList
     */
    @Override
    protected void setPlaylist(List<Song> songList) {
        // create a shallow copy of our playlist, so we have our own copy in Playlist to deal with
        // O(n) time
        List<Song> songListShuffled = new ArrayList<Song>(songList);
        // then sort the playlist, O(n) time
        Collections.shuffle(songListShuffled);
        m_songList = songListShuffled;
    }

}

package me.jvt.mdp.amp;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import me.jvt.mdp.amp.model.Song;

/**
 * Our main activity, which also provides controls for the music, such as playing and seeking
 */
public class ControlActivity extends AppCompatActivity {

    private MusicService.MusicBinder m_musicService = null;
    private Button m_btnPlayPause;
    private Button m_btnStop;
    private TextView m_txvNowPlayingTitle;
    private TextView m_txvNowPlayingArtist;
    private Switch m_swShuffle;
    private SeekBar m_skbSeek;
    private TextView m_txvDurationStr;
    private Handler m_handler = new Handler();
    private ServiceConnection m_serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Log.d(ApplicationConfiguration.TAG, "MusicService connected to ControlActivity");
            m_musicService = (MusicService.MusicBinder) service;
            updateActivityContent();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(ApplicationConfiguration.TAG, "MusicService disconnected from ControlActivity");
            m_musicService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controls);

        // initialise all our widgets so we can use them later in the code
        m_btnPlayPause = (Button) findViewById(R.id.btnPlayPause);
        m_btnStop = (Button) findViewById(R.id.btnStop);
        m_txvNowPlayingTitle = (TextView) findViewById(R.id.txvNowPlayingTitle);
        m_txvNowPlayingArtist = (TextView) findViewById(R.id.txvNowPlayingArtist);
        m_swShuffle = (Switch) findViewById(R.id.swShuffle);
        m_skbSeek = (SeekBar) findViewById(R.id.skbSeek);
        m_txvDurationStr = (TextView) findViewById(R.id.txvDurationStr);

        // add a listener for when our switch is changed - unfortunately we can't do this with a
        // method, or at least, like how button clicks can be handled
        m_swShuffle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isShuffle) {
                if (m_musicService != null) {
                    m_musicService.shuffle(isShuffle);
                    Switch shuffle = (Switch) compoundButton;
                    shuffle.setChecked(isShuffle);
                }
            }
        });

        // shamelessly adapted from http://stackoverflow.com/a/17171025
        // my way of doing this would have involved the Observer pattern - not very nice at all!
        ControlActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (m_musicService != null) {
                    updateSeekBar();
                    if (m_musicService.isPlaying()) {
                        Song currentSong = m_musicService.getCurrentSong();
                        if (currentSong != null) {
                            int curPos = m_musicService.getCurrentPosition();
                            int duration = currentSong.getDuration();
                            // curPos will continue incrementing, so the chance of them equalling each
                            // other is incredibly slim
                            // I've also found that some songs finish within a half second of their full
                            // duration, therefore we need to give them a little leeway
                            if (curPos + 500 >= duration) {
                                m_musicService.nextSong();
                                updateActivityContent();
                            }
                        }
                    }
                }
                // every second is a good time - doesn't make it too often to cause i.e. lag, but
                // is often enough to give a nice UX
                m_handler.postDelayed(this, 1000);
            }
        });

        // listen for the user playing with the seek bar
        m_skbSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean wasUser) {
                if (wasUser) {
                    m_musicService.setCurrentPosition(progress * 1000);
                    updateSeekBar();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // don't perform anything useful here, as we only care about when the bar has
                // changed
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // don't perform anything useful here, as we only care about when the bar has
                // changed
            }
        });

        bindService(new Intent(this, MusicService.class), m_serviceConnection, BIND_AUTO_CREATE);
        updateActivityContent();
        firstRun();
    }

    /**
     * On our first run, we want to welcome the user, and run our database update in the background,
     * so we direct them to the button on the ListMusic activity, and once they return, we'll set
     * it as being complete
     */
    private void firstRun() {
        // adapted from http://stackoverflow.com/a/7563895
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun) {

            startActivity(new Intent(ControlActivity.this, ListMusicActivity.class));

            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRun", false)
                    .apply();
        }
    }

    /**
     * Update our SeekBar, taking care to disable it when we do not have music playing, and setting
     * the correct limits and position when we do. Also display the duration nicely, so the user
     * know far they have been seeking
     */
    private void updateSeekBar() {
        if (!m_musicService.isPlaying())
            return;

        Song currentSong = m_musicService.getCurrentSong();
        if (currentSong != null) {
            int durationMs = currentSong.getDuration();
            int currentPositionMs = m_musicService.getCurrentPosition();

            m_skbSeek.setEnabled(true);
            m_skbSeek.setMax(durationMs / 1000);
            m_skbSeek.setProgress(currentPositionMs / 1000);
            m_txvDurationStr.setText(Song.getDurationStr(currentPositionMs) + "/" + Song.getDurationStr(durationMs));
        } else {
            m_skbSeek.setEnabled(false);
            m_txvDurationStr.setText("");

        }
    }

    // when we restart or resume, make sure to update all the page's content
    @Override
    protected void onRestart() {
        super.onRestart();
        updateActivityContent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateActivityContent();
    }

    /**
     * Update the user's view of the current song; namely the title and artist, and enable/disable
     * the buttons depending on what is happening
     */
    public void updateActivityContent() {
        // if we don't have music playing, disable the buttons, and set the text to defaults
        if (m_musicService == null || m_musicService.isStopped()) {
            m_btnPlayPause.setEnabled(false);
            m_btnStop.setEnabled(false);
            m_txvNowPlayingTitle.setText(R.string.txvNowPlayingTitle);
            m_txvNowPlayingArtist.setText(R.string.txvNowPlayingArtist);
            return;
        }

        Song currentSong = m_musicService.getCurrentSong();

        m_txvNowPlayingTitle.setText(currentSong.getTitle());
        m_txvNowPlayingArtist.setText(currentSong.getArtist());

        m_btnPlayPause.setEnabled(true);
        m_btnStop.setEnabled(true);
        // swap between the button saying (Play|Pause) depending on whether the music is paused or
        // playing, respectively
        if (m_musicService.isPlaying()) {
            m_btnPlayPause.setText(R.string.btnPause);
        } else {
            m_btnPlayPause.setText(R.string.btnPlay);

        }
    }

    public void btnPlayPauseClick(View v) {
        m_musicService.pause();
        updateActivityContent();
    }

    public void btnStopClick(View v) {
        m_musicService.stop();
        updateActivityContent();
    }

    public void btnNextClick(View v) {
        m_musicService.nextSong();
        updateActivityContent();
    }

    public void btnPreviousClick(View v) {
        m_musicService.previousSong();
        updateActivityContent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(m_serviceConnection);
    }

    public void btnListMusic(View v) {
        Intent intent = new Intent(this, ListMusicActivity.class);
        startActivity(intent);
    }
}

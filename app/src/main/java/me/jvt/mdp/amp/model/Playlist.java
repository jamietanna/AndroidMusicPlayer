package me.jvt.mdp.amp.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import me.jvt.mdp.amp.ApplicationConfiguration;

/**
 * A container for our playlist of music, which provides wrapper functions to control the playlist,
 * such as moving to the next song
 */
public class Playlist {
    protected List<Song> m_songList = null;
    protected int m_currPos = 0;

    public Playlist() {
        setPlaylist(new ArrayList<Song>());
    }

    public Playlist(List<Song> songList) {
        setPlaylist(songList);
    }

    public Song getSongAtPosition(int pos) {
        return m_songList.get(pos);
    }

    public Song getCurrentSong() {
        return getSongAtPosition(m_currPos);
    }

    protected int getPlaylistLength() {
        return m_songList.size();
    }

    protected void setPlaylist(List<Song> songList) {
        // perform a shallow copy of the ArrayList so we have our own copy of the data to manipulate
        // as needbe
        m_songList = new ArrayList<Song>(songList);
    }

    public Song goToNextSong() {
        moveToPosition(m_currPos + 1);
        return getCurrentSong();
    }

    public Song goToPreviousSong() {
        moveToPosition(m_currPos - 1);
        return getCurrentSong();
    }

    public void moveToPosition(int newPos) {
        int playlistLength = getPlaylistLength();

        // make sure we wrap around to avoid hitting an index higher than we have in our list
        if (newPos >= playlistLength)
            newPos = 0;

        // make sure we wrap around to avoid hitting an index lower than we have in our list
        if (newPos < 0)
            newPos = playlistLength - 1;

        m_currPos = newPos;
    }

    /**
     * Given a song, work out what position we are in the playlist - useful when swapping between
     * playlists, but we want to keep the current song!
     *
     * @param song
     * @return
     */
    public int getPositionOfSong(Song song) {
        int position = 0;
        // only update position if we've actually got a previous playlist; otherwise just start
        // at the beginning!
        for (position = 0; position < m_songList.size(); position++) {
            Song currentSong = m_songList.get(position);
            Log.d(ApplicationConfiguration.TAG, "cur " + currentSong + " vs " + song);
            if (currentSong.equals(song)) {
                break;
            }
        }
        return position;
    }

}

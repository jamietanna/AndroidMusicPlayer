package me.jvt.mdp.amp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.List;

import me.jvt.mdp.amp.db.MusicDBHelper;
import me.jvt.mdp.amp.model.Playlist;
import me.jvt.mdp.amp.model.ShufflePlaylist;
import me.jvt.mdp.amp.model.Song;

/**
 * Our bound Service which actually handles the playing of music, as well as storing the current
 * playlist that the app will be looping through
 */
public class MusicService extends Service {

    private final IBinder binder = new MusicBinder();
    private AudioPlayer m_audioPlayer = AudioPlayer.getInstance();
    private MusicDBHelper m_musicDBHelper;
    private Song m_currentSong;
    private Playlist m_playlist;
    /**
     * Store the actual list of songs we have on the device in memory, instead of having to grab
     * them from the database each time we switch between playing sequentially and m_shuffled
     * TODO this isn't necessary?!
     */
    private List<Song> m_baseSongList;
    private boolean m_shuffled;

    @Override
    public void onCreate() {
        super.onCreate();
        m_musicDBHelper = new MusicDBHelper(this);
        refreshPlaylist();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public boolean isShuffled() {
        return m_shuffled;
    }

    /**
     * Reload our playlist by querying our songs, and recreating the Playlist
     */
    private void refreshPlaylist() {
        m_baseSongList = m_musicDBHelper.getAllSongs();
        shuffle();
    }

    private int getCurrentPosition() {
        return m_audioPlayer.getCurrentPosition();
    }

    private void setCurrentPosition(int currentPosition) {
        m_audioPlayer.setCurrentPosition(currentPosition);
    }

    /**
     * Flip between shuffle and sequential playback.
     * <p/>
     * This method takes care to ensure that the currently playing song when the shuffle occurs
     * remains in the correct place when switching, so as not to confuse users when they:
     * <p/>
     * 1. hit shuffle
     * 2. hit next
     * 3. hit previous
     * 4. find a different song waiting for them
     */
    private void shuffle() {
        // get the position of getCurrentSong()
        // shuffle -> seq
        //      find out what pos our Song should be at
        //      go there
        // seq -> shuffle
        //      make sure that Song is the current one (`[0]`th)
        if (!m_shuffled) {
            // if we're going from shuffle to sequential
            int position = 0;
            if (m_playlist != null && isPlaying())
                position = m_playlist.getPositionOfSong(getCurrentSong());
            m_playlist = new Playlist(m_baseSongList);
            m_playlist.moveToPosition(position);
        } else {
            m_playlist = new ShufflePlaylist(getCurrentSong(), m_baseSongList);
        }
    }

    private void nextSong() {
        m_currentSong = m_playlist.goToNextSong();
        playSong(m_currentSong);
    }

    private void previousSong() {
        // after ~5 seconds of playback, it could be that the user wanted to go back to the
        // previous song
        long pos = m_audioPlayer.getCurrentPosition();
        if (pos <= 5000) {
            m_currentSong = m_playlist.goToPreviousSong();
        }

        playSong(m_currentSong);
    }

    private void play() {
        m_audioPlayer.pause();
    }

    private void pause() {
        m_audioPlayer.pause();
    }

    private void stop() {
        m_audioPlayer.stop();
    }

    private boolean isPlaying() {
        return m_audioPlayer.isPlaying();
    }

    private boolean isStopped() {
        return m_audioPlayer.isStopped();
    }

    private void playSong(Song songToPlay) {

        // TODO why does this work?!?!
        if (m_currentSong == null || !m_currentSong.equals(songToPlay)) {
            int pos = m_playlist.getPositionOfSong(songToPlay);
            m_playlist.moveToPosition(pos);
        }
        m_currentSong = m_playlist.getCurrentSong();
        m_audioPlayer.play(this, getCurrentSong().getFilePath());
    }

    private Song getCurrentSong() {
        if (m_currentSong == null)
            m_currentSong = m_playlist.getCurrentSong();
        return m_currentSong;
    }

    /**
     * Our binder provides a nice interface to external Activities in which to query the Service
     * and access the internal media player
     */
    public class MusicBinder extends Binder {

        public void play() {
            MusicService.this.pause();
        }

        public void pause() {
            MusicService.this.pause();
        }

        public void stop() {
            MusicService.this.stop();
        }

        public boolean isPlaying() {
            return MusicService.this.isPlaying();
        }

        public boolean isStopped() {
            return MusicService.this.isStopped();
        }

        public void playFile(Song song) {
            MusicService.this.playSong(song);
        }

        public Song getCurrentSong() {
            return MusicService.this.getCurrentSong();
        }

        public void nextSong() {
            MusicService.this.nextSong();
        }

        public void previousSong() {
            MusicService.this.previousSong();
        }

        public void shuffle(boolean isShuffle) {
            m_shuffled = isShuffle;
            MusicService.this.shuffle();
        }

        public int getCurrentPosition() {
            return MusicService.this.getCurrentPosition();
        }

        public void setCurrentPosition(int currentPosition) {
            MusicService.this.setCurrentPosition(currentPosition);
        }

        public void refreshPlaylist() {
            MusicService.this.refreshPlaylist();
        }

        public boolean isShuffled() {
            return MusicService.this.isShuffled();
        }
    }

}

package me.jvt.mdp.amp.db;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import me.jvt.mdp.amp.ApplicationConfiguration;
import me.jvt.mdp.amp.model.Song;

/**
 * A helper class which performs a lot of the heavy lifting when working with the database. It also
 * provides a nicer interface than raw SQL queries.
 */
public class MusicDBHelper extends SQLiteOpenHelper {

    // store globals so we don't have to remember what position everything is in. it also helps
    // maintenance
    public static final int SCHEMA_FILEHASH_POS = 1;
    public static final int SCHEMA_FILENAME_POS = 2;
    public static final int SCHEMA_ARTIST_POS = 3;
    public static final int SCHEMA_ALBUM_POS = 4;
    public static final int SCHEMA_TITLE_POS = 5;
    public static final int SCHEMA_DURATION_POS = 6;
    public static final String SCHEMA_FILEHASH_NAME = "fileHash";
    public static final String SCHEMA_FILENAME_NAME = "fileName";
    public static final String SCHEMA_ARTIST_NAME = "artist";
    public static final String SCHEMA_ALBUM_NAME = "album";
    public static final String SCHEMA_TITLE_NAME = "title";
    public static final String SCHEMA_DURATION_NAME = "duration";
    static final String DB_NAME = "amp";
    static final int DB_VERSION = 4;

    public MusicDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /**
     * Query the Media ContentProvider and create a List<Song> for us to work with
     *
     * @param applicationContext
     * @return
     */
    public static List<Song> queryMediaContentProvider(Context applicationContext) {
        // adapted from http://stackoverflow.com/a/21333187
        ContentResolver cr = applicationContext.getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cur = cr.query(uri, null, selection, null, sortOrder);

        ArrayList<Song> alSongs = new ArrayList<Song>();

        if (cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                alSongs.add(new Song(
                        cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)),
                        cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)),
                        cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE)),
                        cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA)),
                        cur.getInt(cur.getColumnIndex(MediaStore.Audio.Media.DURATION))
                ));
            }
        }

        cur.close();

        return alSongs;
    }

    /**
     * Helper function to return a Song when given a Cursor - just cuts down repetition of code
     *
     * @param cursor
     * @return
     */
    public static Song getSongFromCursor(Cursor cursor) {
        return new Song(
                cursor.getString(SCHEMA_ARTIST_POS),
                cursor.getString(SCHEMA_ALBUM_POS),
                cursor.getString(SCHEMA_TITLE_POS),
                cursor.getString(SCHEMA_FILENAME_POS),
                cursor.getInt(SCHEMA_DURATION_POS)
        );
    }

    /**
     * Run a full refresh of our database via the Android Media ContentProvider
     *
     * @param context
     */
    public void updateDB(Context context) {
        refreshDatabase();
        List<Song> alSongs = queryMediaContentProvider(context);
        for (Song currSong : alSongs) {
            boolean didInsert = insertRow(currSong);
            if (!didInsert)
                Log.e(ApplicationConfiguration.TAG, "Could not insert " + currSong.getFilePath() + " into the database");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTable(db);
    }

    /**
     * Insert our Songs into the database
     *
     * @param song
     * @return success status
     */
    public boolean insertRow(Song song) {
        SQLiteDatabase db = getWritableDatabase();

        // http://stackoverflow.com/a/9284092
        String fileHash = Song.computeFileHash(song);

        String sqlInsert = "INSERT INTO " + DB_NAME + " (" +
                SCHEMA_FILEHASH_NAME +
                "," +
                SCHEMA_FILENAME_NAME +
                "," +
                SCHEMA_ARTIST_NAME +
                "," +
                SCHEMA_ALBUM_NAME +
                "," +
                SCHEMA_TITLE_NAME +
                "," +
                SCHEMA_DURATION_NAME +
                ") VALUES (?,?,?,?,?,?);";

        SQLiteStatement stmt = db.compileStatement(sqlInsert);
        // SQLite prepared statements are 1-indexed [https://www.sqlite.org/c3ref/bind_blob.html]
        stmt.bindString(1, fileHash);
        stmt.bindString(2, song.getFilePath());
        stmt.bindString(3, song.getArtist());
        stmt.bindString(4, song.getAlbum());
        stmt.bindString(5, song.getTitle());
        stmt.bindLong(6, song.getDuration());

        long lastRow = stmt.executeInsert();
        // lastRow returns -1 if there has been an error
        return lastRow != -1;
    }

    protected void dropTable(SQLiteDatabase db) {
        SQLiteStatement stmt = db.compileStatement("DROP TABLE IF EXISTS " + DB_NAME + ";");
        stmt.execute();
    }

    /**
     * Drop, and then recreate our database
     */
    protected void refreshDatabase() {
        SQLiteDatabase db = getWritableDatabase();
        dropTable(db);
        createTable(db);
    }

    /**
     * Initialise our table
     *
     * @param db
     */
    protected void createTable(SQLiteDatabase db) {
        SQLiteStatement stmt = db.compileStatement("CREATE TABLE " + DB_NAME +
                " " +
                "(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT" +
                "," +
                SCHEMA_FILEHASH_NAME +
                " " +
                "VARCHAR(32)" +
                "," +
                SCHEMA_FILENAME_NAME +
                " " +
                "VARCHAR(256) " +
                "," +
                SCHEMA_ARTIST_NAME +
                " " +
                "VARCHAR(64) " +
                "," +
                SCHEMA_ALBUM_NAME +
                " " +
                "VARCHAR(64) " +
                "," +
                SCHEMA_TITLE_NAME +
                " " +
                "VARCHAR(64) " +
                "," +
                SCHEMA_DURATION_NAME +
                " " +
                "VARCHAR(64) " +
                ");");
        stmt.execute();
    }

    /**
     * Query the database and get a Cursor to play with
     *
     * @return
     */
    public Cursor getAllRows() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT " +
                "rowid _id" +
                "," +
                " " +
                SCHEMA_FILEHASH_NAME +
                "," +
                SCHEMA_FILENAME_NAME +
                "," +
                SCHEMA_ARTIST_NAME +
                "," +
                SCHEMA_ALBUM_NAME +
                "," +
                SCHEMA_TITLE_NAME +
                "," +
                SCHEMA_DURATION_NAME +
                " " +
                "FROM " +
                DB_NAME +
                ";", null);
    }

    /**
     * Query the database for all our Songs
     *
     * @return
     */
    public List<Song> getAllSongs() {
        Cursor cur = getAllRows();
        List<Song> alSongs = new ArrayList<Song>();

        if (cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                alSongs.add(getSongFromCursor(cur));
            }
        }

        return alSongs;
    }

}

package me.jvt.mdp.amp;

public class ApplicationConfiguration {
    /**
     * TAG is the tag for Logcat - so we can more easily track down our application's debug logging
     */
    public static final String TAG = "AMP";

    private ApplicationConfiguration() {
        // ApplicationConfiguration is used for configuration, and any other shared items - we don't want anyone
        // to be able to create an instance, so override the implicit constructor
    }
}

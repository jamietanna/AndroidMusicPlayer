package me.jvt.mdp.amp;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import me.jvt.mdp.amp.db.MusicDBHelper;
import me.jvt.mdp.amp.model.Song;

/**
 * Activity which displays to the user their music library, letting them choose the songs they want
 * to play
 */
public class ListMusicActivity extends AppCompatActivity {

    private MusicDBHelper m_dbHelper;
    private ListView m_lvMusicList;
    private TextView m_txvFirstRun;
    private MusicService.MusicBinder m_musicService = null;
    private ListView.OnItemClickListener m_lvMusicListClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            // grab the cursor at the correct position the user clicked
            SQLiteCursor cursor = (SQLiteCursor) adapter.getItemAtPosition(position);
            // parse the Song() we're going to be playing
            Song nextSong = MusicDBHelper.getSongFromCursor(cursor);
            // then play it!
            m_musicService.playFile(nextSong);
        }
    };
    private ServiceConnection m_serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Log.d(ApplicationConfiguration.TAG, "MusicService connected to ListMusicActivity");
            m_musicService = (MusicService.MusicBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(ApplicationConfiguration.TAG, "MusicService disconnected from ListMusicActivity");
            m_musicService = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        this.bindService(new Intent(this, MusicService.class), m_serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_music);

        m_lvMusicList = (ListView) findViewById(R.id.lvMusicList);
        m_txvFirstRun = (TextView) findViewById(R.id.txvFirstRun);

        m_dbHelper = new MusicDBHelper(getApplicationContext());

        updateMusicListView();
        m_lvMusicList.setOnItemClickListener(m_lvMusicListClickListener);

        // this is a really horrible hack, but unfortunately we can't work without an SD card!
        if (!android.os.Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No SD Card detected");
            builder.setMessage("No SD card could be detected, and as such the application won't be "
                    + "able to continue. Please insert one and restart the app.");
            // don't let the user dismiss it, so they can't use the app
            builder.setCancelable(false);
            builder.create().show();
        }

    }

    /**
     * Update the user's list of music with the music we've stored in the database
     */
    public void updateMusicListView() {
        // http://developer.android.com/guide/topics/ui/declaring-layout.html#AdapterViews
        String[] fromColumns = {MusicDBHelper.SCHEMA_TITLE_NAME, MusicDBHelper.SCHEMA_ARTIST_NAME, MusicDBHelper.SCHEMA_DURATION_NAME};
        int[] toViews = {R.id.tvMusicItemTitle, R.id.tvMusicItemArtist, R.id.tvMusicItemDuration};

        Cursor cursor = m_dbHelper.getAllRows();

        // we want to have a custom binding here to convert from the integer representation of the
        // duration of a Song() to that of a pretty-printed string such as "12:32"
        // adapted from http://stackoverflow.com/a/5573609
        SimpleCursorAdapter.ViewBinder binder = new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnId) {
                if (columnId == MusicDBHelper.SCHEMA_DURATION_POS) {
                    int duration = cursor.getInt(columnId);
                    String durationStr = Song.getDurationStr(duration);

                    TextView tvMusicItemDuration = (TextView) view.findViewById(R.id.tvMusicItemDuration);
                    tvMusicItemDuration.setText(durationStr);
                    return true;
                }
                return false;
            }
        };

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.music_item_layout, cursor, fromColumns, toViews, 0);
        adapter.setViewBinder(binder);
        m_lvMusicList.setAdapter(adapter);
    }

    public void btnUpdateDBClick(View v) {
        m_dbHelper.updateDB(this);
        m_txvFirstRun.setText(R.string.first_run_complete);
        updateMusicListView();

        m_musicService.refreshPlaylist();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(m_serviceConnection);
    }
}
